const USER = 'user'
const SERVER = 'server'

export default class StorageService {
  static saveUser(user) {
    localStorage.setItem(USER, JSON.stringify(user))
  }

  static loadUser() {
    return JSON.parse(localStorage.getItem(USER))
  }

  static clearUser() {
    localStorage.removeItem(USER)
  }

  static saveServer(serverId) {
    localStorage.setItem(SERVER, serverId)
  }

  static loadServer() {
    return localStorage.getItem(SERVER)
  }

  static clearServer() {
    localStorage.removeItem(SERVER)
  }
}