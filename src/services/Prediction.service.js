import {ConfigEnum, ConfigService} from "./Config.service";

export class PredictionService {
  static async get(serverId, token) {
    const apiUrl = await ConfigService.get(ConfigEnum.PREDICTION_API_URL)
    return fetch(`${apiUrl}/prediction/${serverId}`, {
      headers: {
        Accept: 'application/json',
        Authorization: "Bearer " + token,
      }
    })
  }
}