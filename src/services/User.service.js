import {ConfigEnum, ConfigService} from "./Config.service";

export default class UserService {
  static async login(username, password) {
    const apiUrl = await ConfigService.get(ConfigEnum.USER_API_URL)
    return fetch(`${apiUrl}/auth/login`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        username: username,
        password: password,
      })
    })
  }

  static async register(username, password, email) {
    const apiUrl = await ConfigService.get(ConfigEnum.USER_API_URL)
    return fetch(`${apiUrl}/auth/register`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        username: username,
        password: password,
        email: email,
      })
    })
  }
}