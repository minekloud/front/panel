import {ConfigEnum, ConfigService} from "./Config.service";

export class ServerService {
  static async create(userId, name, hostname, ram, token) {
    const apiUrl = await ConfigService.get(ConfigEnum.SERVER_API_URL)
    return fetch(`${apiUrl}/servers`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-type': 'application/json',
        Authorization: "Bearer " + token,
      },
      body: JSON.stringify({
        idUser: userId,
        name: name,
        hostname: hostname,
        maxAvailableRAM: ram
      })
    })
  }

  static async getList(userId, token) {
    const apiUrl = await ConfigService.get(ConfigEnum.SERVER_API_URL)
    return fetch(`${apiUrl}/servers?idUser=${userId}`, {
      headers: {
        Accept: 'application/json',
        Authorization: "Bearer " + token,
      }
    })
  }

  static async get(serverId, token) {
    const apiUrl = await ConfigService.get(ConfigEnum.SERVER_API_URL)
    return fetch(`${apiUrl}/servers/${serverId}`, {
      headers: {
        Accept: 'application/json',
        Authorization: "Bearer " + token,
      }
    })
  }

  static async update(serverId, update, token) {
    const apiUrl = await ConfigService.get(ConfigEnum.SERVER_API_URL)
    return fetch(`${apiUrl}/servers/${serverId}`, {
      method: 'PATCH',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: "Bearer " + token,
      },
      body: JSON.stringify(update),
    })
  }

  static async delete(serverId, token) {
    const apiUrl = await ConfigService.get(ConfigEnum.SERVER_API_URL)
    return fetch(`${apiUrl}/servers/${serverId}`, {
      method: 'DELETE',
      headers: {
        Accept: 'application/json',
        Authorization: "Bearer " + token,
      }
    })
  }

  static async getListUsers(serverId, token){
    const apiUrl = await ConfigService.get(ConfigEnum.SERVER_API_URL)
    return fetch(`${apiUrl}/servers/${serverId}/players`, {
      method: 'GET',
      headers : {
        Authorization: "Bearer " + token
      }
    })
  }

  static async getPlayerAvatar(playerUsername){
    const apiUrl = await ConfigService.get(ConfigEnum.PLAYERS_SKIN_API_URL)
    return fetch(`${apiUrl}/${playerUsername}`,{
      method: 'GET',
      
    })
  }

  static async sendCommand(serverId, command, token) {
    const apiUrl = await ConfigService.get(ConfigEnum.SERVER_API_URL)
    return fetch(`${apiUrl}/servers/${serverId}/sendcommand`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: "Bearer " + token,
      },
      body: JSON.stringify({
        command: command
      })
    })
  }
}

export const ServerStatus = {
  OFF: 'off',
  ON: 'on',
}