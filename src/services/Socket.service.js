import {ConfigEnum, ConfigService} from "./Config.service";
import io from "socket.io-client";

export default class SocketService {
  static logSocket = null
  static metricsSocket = null

  static async connectLogSocket(userId, serverId, token) {
    SocketService.disconnectLogSocket()
    const apiUrl = await ConfigService.get(ConfigEnum.LOG_API_URL)
    SocketService.logSocket = io(apiUrl, {
      path: '/api/logging/socket.io',
      query: {
        userId: userId,
        serverId: serverId,
        token: token,
      },
    })
    return SocketService.logSocket
  }

  static disconnectLogSocket() {
    SocketService.logSocket?.disconnect()
    SocketService.logSocket = null
  }

  static async connectMetricsSocket(userId, serversId, token) {
    SocketService.disconnectMetricsSocket()
    const apiUrl = await ConfigService.get(ConfigEnum.METRICS_API_URL)
    SocketService.metricsSocket = io(apiUrl, {
      path: '/api/metrics/live/socket.io',
      query: {
        userId: userId,
        serversId: serversId,
        token: token,
      },
    })
    return SocketService.metricsSocket
  }

  static disconnectMetricsSocket() {
    SocketService.metricsSocket?.disconnect()
    SocketService.metricsSocket = null
  }
}