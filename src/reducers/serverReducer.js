import {ACTION_SELECT, ACTION_UNSELECT, ACTION_UPDATE} from "../actions/serverActions";
import StorageService from "../services/Storage.service";

const initialState = {
  value: null
}

export default function serverReducer(state = initialState, action) {
  switch (action.type) {
    case ACTION_SELECT:
      StorageService.saveServer(action.value.id)
    case ACTION_UPDATE:
      return {
        ...state,
        value: action.value,
      }
    case ACTION_UNSELECT:
      StorageService.clearServer()
      return {
        ...state,
        value: null,
      }
    default:
      return state
  }
}