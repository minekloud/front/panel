import {ACTION_CLEAR, ACTION_FETCH, ACTION_UPDATE, ACTION_UPDATE_SERVER} from "../actions/serverListActions";

const initialState = {
  value: null,
  isUpdating: true
}

export default function serverListReducer(state = initialState, action) {
  switch (action.type) {
    case ACTION_UPDATE:
      return {
        ...state,
        value: action.value,
        isUpdating: false
      }
    case ACTION_FETCH:
      return {
        ...state,
        isUpdating: true
      }
    case ACTION_UPDATE_SERVER:
      return {
        ...state,
        value: state.value?.map(x => x.id === action.value.id ? action.value : x) || null
      }
    case ACTION_CLEAR:
      return {
        ...state,
        value: null,
        isUpdating: true
      }
    default:
      return state
  }
}