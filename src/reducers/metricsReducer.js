import {ACTION_CONNECT, ACTION_DISCONNECT, ACTION_UPDATE} from "../actions/metricsActions";

const initialState = {
  connected: false,
  serverList: [],
  health: [],
}

export default function metricsReducer(state = initialState, action) {
  switch (action.type) {
    case ACTION_CONNECT:
      return {
        ...state,
        connected: true,
        serverList: action.value,
        health: updateHealth(state.health),
      }
    case ACTION_DISCONNECT:
      return {
        ...state,
        connected: false,
        serverList: [],
        health: updateHealth(state.health),
      }
    case ACTION_UPDATE:
      const health = updateHealth(state.health)
      action.value.forEach(val => {
        const index = health.findIndex(h => h.serverId === val.serverId)
        if (index > -1) {
          health[index] = val
        } else {
          health.push(val)
        }
      })
      return {
        ...state,
        health: health
      }
    default:
      return state
  }
}

function updateHealth(health) {
  return health.filter(h => h.timestamp + 6 > Date.now() / 1000)
}