import {combineReducers, createStore} from "redux";
import userReducer from "./userReducer";
import serverReducer from "./serverReducer";
import serverListReducer from "./serverListReducer";
import alertReducer from "./alertReducer"
import logReducer from "./logReducer";
import metricsReducer from "./metricsReducer";

const reducers = combineReducers({
  userReducer,
  serverReducer,
  serverListReducer,
  alertReducer,
  logReducer,
  metricsReducer,
})

function configureStore() {
  return createStore(reducers, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__({trace: true}));
}

export const store = configureStore();