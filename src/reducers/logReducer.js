import {ACTION_CLEAR_LOG, ACTION_NEW_LOG, ACTION_OLD_LOG} from "../actions/logActions";

const initialState = {
  value: []
}

export default function logReducer(state = initialState, action) {
  switch (action.type) {
    case ACTION_NEW_LOG:
      return {
        ...state,
        value: [...state.value, ...action.value],
      }
    case ACTION_OLD_LOG:
      return {
        ...state,
        value: [...action.value, ...state.value],
      }
    case ACTION_CLEAR_LOG:
      return {
        ...state,
        value: [],
      }
    default:
      return state
  }
}