export const ACTION_NEW_LOG = 'log/new'
export const ACTION_OLD_LOG = 'log/old'
export const ACTION_CLEAR_LOG = 'log/clear'

export const newLogAction = value => {
  return {
    type: ACTION_NEW_LOG,
    value: value,
  }
}

export const oldLogAction = value => {
  return {
    type: ACTION_OLD_LOG,
    value: value,
  }
}

export const clearLogAction = () => {
  return {
    type: ACTION_CLEAR_LOG,
  }
}
