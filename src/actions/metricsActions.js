export const ACTION_CONNECT = "metrics/connect"
export const ACTION_DISCONNECT = "metrics/disconnect"
export const ACTION_UPDATE = "metrics/update"

export const connectMetrics = value => {
  return {
    type: ACTION_CONNECT,
    value: value
  }
}

export const disconnectMetrics = () => {
  return {
    type: ACTION_DISCONNECT,
  }
}

export const updateMetricsAction = value => {
  return {
    type: ACTION_UPDATE,
    value: value,
  }
}