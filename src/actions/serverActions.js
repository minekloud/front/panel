export const ACTION_SELECT = 'server/select'
export const ACTION_UPDATE = 'server/update'
export const ACTION_UNSELECT = 'server/unselect'

export const selectServerAction = value => {
  return {
    type: ACTION_SELECT,
    value: value,
  }
}

export const updateServerAction = value => {
  return {
    type: ACTION_UPDATE,
    value: value,
  }
}

export const unselectServerAction = () => {
  return {
    type: ACTION_UNSELECT,
  }
}