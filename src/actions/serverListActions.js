export const ACTION_UPDATE = 'serverList/update'
export const ACTION_FETCH = 'serverList/fetch'
export const ACTION_UPDATE_SERVER = 'serverList/updateServer'
export const ACTION_CLEAR = 'serverList/clear'

export const updateServerListAction = value => {
  return {
    type: ACTION_UPDATE,
    value: value,
  }
}

export const fetchServerListAction = () => {
  return {
    type: ACTION_FETCH,
  }
}

export const updateServerInListAction = value => {
  return {
    type: ACTION_UPDATE_SERVER,
    value: value,
  }
}

export const clearServerListAction = () => {
  return {
    type: ACTION_CLEAR,
  }
}
