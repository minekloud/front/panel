export const ACTION_LOGIN = 'user/login'
export const ACTION_LOGOUT = 'user/logout'

export const loginAction = value => {
  return {
    type: ACTION_LOGIN,
    value: value,
  }
}

export const logoutAction = () => {
  return {
    type: ACTION_LOGOUT,
  }
}