import {Grid, IconButton, makeStyles, Switch, TableCell, TableRow, Tooltip, Typography} from '@material-ui/core';
import React, {useState} from 'react';
import {FileCopy} from "@material-ui/icons";
import {useDispatch, useSelector} from "react-redux";
import {selectServerAction} from "../../../actions/serverActions";
import {useHistory} from "react-router-dom";
import {ServerService} from "../../../services/Server.service";
import {updateServerInListAction} from "../../../actions/serverListActions";
import APIService from "../../../services/API.service";
import {getStatus, Status} from "../../../services/Metrics.service";
import ServerStatusIndicator from "../server/status/ServerStatusIndicator";

const useStyle = makeStyles(() => ({
  serverRow: {
    cursor: "pointer"
  },
  statusIndicatorBox: {
    display: "inline-block"
  }
}));

const defaultTooltipText = "Copy address to clipboard"

export function ServerRow(props) {
  const server = props.server
  const loggedUser = useSelector((state) => state.userReducer).value
  const health = useSelector(state => state.metricsReducer.health).find(h => h.serverId === server.id)
  const [tooltipText, setTooltipText] = useState(defaultTooltipText)
  const dispatch = useDispatch()
  const history = useHistory()
  const classes = useStyle()

  const handleChange = async (event) => {
    const newStatus = event.target.checked ? 'on' : 'off'
    APIService.fetchJson(dispatch, ServerService.update(server.id, {status: newStatus}, loggedUser.token))
      .success(async (_, json) => await json().then(res => dispatch(updateServerInListAction(res))))
      .error(() => {
        throw new Error("A server error occured while updating the server.")
      })
  }

  const handleServerSelection = () => {
    dispatch(selectServerAction(server))
    history.push("/panel/server")
  }

  const handleCopy = (e) => {
    e.stopPropagation()
    navigator.clipboard.writeText(server.hostname + ".play.minekloud.com")
    setTooltipText("Copied!")
  }

  const status = getStatus(server.status, health?.status)
  return (
    <TableRow hover={true} onClick={handleServerSelection} className={classes.serverRow}>
      <TableCell>#{server.id}</TableCell>
      <TableCell><b>{server.name}</b></TableCell>
      <TableCell>
        {server.hostname}
        <Tooltip title={tooltipText} arrow placement="top" onClick={handleCopy}
                 onClose={() => setTooltipText(defaultTooltipText)}>
          <IconButton>
            <FileCopy/>
          </IconButton>
        </Tooltip>
      </TableCell>
      <TableCell>
        <div className={classes.statusIndicatorBox}>
          <ServerStatusIndicator status={status} size="small"/>
        </div>
      </TableCell>
      <TableCell>{server.maxAvailableRAM} GB</TableCell>
      <TableCell>
        <Typography component="div">
          <Grid component="label" container alignItems="center" spacing={1}>
            <Grid item>Off</Grid>
            <Grid item>
              <Switch onClick={(e) => e.stopPropagation()} onChange={handleChange}
                      checked={server.status === 'on'}
                      name="onOffCheckbox"
                      disabled={status === Status.UNKNOWN}
              />
            </Grid>
            <Grid item>On</Grid>
          </Grid>
        </Typography>
      </TableCell>
    </TableRow>


  );
}

