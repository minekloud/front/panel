import DialogContent from "@material-ui/core/DialogContent";
import TextField from "@material-ui/core/TextField";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions";
import {Button, FormControl, InputLabel, MenuItem, Select, Slider, Typography} from "@material-ui/core";
import Dialog from "@material-ui/core/Dialog";
import React, {useEffect, useState} from "react";
import {ServerService} from "../../../services/Server.service";
import {useDispatch, useSelector} from "react-redux";
import {showSuccessAlertAction} from "../../../actions/alertActions";
import {fetchServerListAction} from "../../../actions/serverListActions";
import {CustomDialogTitle} from "../../CustomDialogTitle";
import APIService from "../../../services/API.service";

export default function CreateServerDialog(props) {
  const loggedUser = useSelector((state) => state.userReducer).value

  const [dialogOpen, setDialogOpen] = useState(false);
  const [name, setName] = useState("")
  const [hostname, setHostname] = useState("")
  const [ram, setRam] = useState(1)
  const [nameError, setNameError] = useState(null)
  const [hostnameError, setHostnameError] = useState(null)
  const [buttonDisabled, setButtonDisabled] = useState(null)

  const dispatch = useDispatch()

  const marks = [
    {
      value: 1,
      label: '1 GB',
    },
    {
      value: 2,
      label: '2 GB',
    },
    {
      value: 3,
      label: '3 GB',
    },
    {
      value: 4,
      label: '4 GB',
    },
  ]

  useEffect(() => {
    if (props.openDialog > 0) {
      handleClickOpenDialog()
    }
  }, [props.openDialog])

  const handleNameChange = (event) => {
    let newServerName = event.target.value
    setName(newServerName)
    if (nameError !== null && name !== newServerName && newServerName !== "") {
      setNameError(null)
    }
  }
  const handleHostnameChange = (event) => {
    let newServerHostname = event.target.value.toLowerCase()
    setHostname(newServerHostname)
    if (hostnameError !== null && hostname !== newServerHostname && newServerHostname !== "") {
      setHostnameError(null)
    }
  }

  const handleRamChange = (_, value) => {
    setRam(value)
  }

  const validateName = () => {
    if (name === "") {
      setNameError("Please enter a server name.")
      return false
    }
    return true
  }

  const validateHostname = () => {
    if (hostname === "") {
      setHostnameError("Please enter a server hostname.")
      return false
    }
    const hostnameRegex = /^[a-z]+[a-z0-9_-]{2,}$/
    if (!hostnameRegex.test(hostname)) {
      setHostnameError("The hostname must be at least 3 characters long and can only contain lowercase letters, numbers, characters \"-\" or \"_\", and must start with a letter.")
      return false
    }
    return true
  }

  const validateForm = () => [validateName(), validateHostname()].every(Boolean)

  const handleCreateServer = async (e) => {
    e.preventDefault()
    clearErrors()
    if (!validateForm()) return
    setButtonDisabled(true)

    APIService.fetchJson(dispatch, ServerService.create(loggedUser.user.id, name, hostname, ram, loggedUser.token))
      .success(onCreateServerSuccess)
      .error(onCreateServerError)
      .finally(() => setButtonDisabled(false))
  }

  const onCreateServerSuccess = () => {
    setDialogOpen(false)
    dispatch(fetchServerListAction())
    dispatch(showSuccessAlertAction("Server successfully created!", 4000))
  }

  const onCreateServerError = async (response, json) => {
    if (response.status === 409) {
      await json().then(err => setHostnameError(err.message))
      return
    }
    throw new Error("A server error occured while creating the server.")
  }

  const handleClickOpenDialog = () => {
    setName("")
    setHostname("")
    setRam(1)
    clearErrors()
    setDialogOpen(true);
  }

  const clearErrors = () => {
    setNameError(null)
    setHostnameError(null)
  }

  const handleCloseDialog = () => {
    setDialogOpen(false);
  };

  return (
    <div>
      <Dialog open={dialogOpen} onClose={handleCloseDialog}>
        <CustomDialogTitle onClose={handleCloseDialog}>
          Create a server
        </CustomDialogTitle>
        <form onSubmit={handleCreateServer}>
          <DialogContent>
            <TextField required fullWidth variant="outlined" margin="normal"
                       value={name} onChange={handleNameChange} label="Name"
                       error={nameError !== null} helperText={nameError}/>
            <TextField required fullWidth variant="outlined" margin="normal"
                       value={hostname} onChange={handleHostnameChange} label="Hostname"
                       error={hostnameError !== null} helperText={hostnameError}/>
            <DialogContentText>
              Server IP : {(hostname || "[hostname]") + ".play.minekloud.com"}
            </DialogContentText>
            <FormControl required variant="outlined" margin="normal" fullWidth>
              <InputLabel>Type</InputLabel>
              <Select value="Vanilla" label="Type *">
                <MenuItem value="Vanilla">Vanilla</MenuItem>
                <MenuItem disabled>Spigot</MenuItem>
                <MenuItem disabled>Paper</MenuItem>
              </Select>
            </FormControl>
            <FormControl required variant="outlined" margin="normal" fullWidth>
              <InputLabel>Version</InputLabel>
              <Select value="1.16.4" label="Version *">
                <MenuItem value="1.16.4">1.16.4</MenuItem>
                <MenuItem disabled>1.16.3</MenuItem>
                <MenuItem disabled>1.16.2</MenuItem>
                <MenuItem disabled>1.16.1</MenuItem>
                <MenuItem disabled>1.16</MenuItem>
              </Select>
            </FormControl>
            <Typography gutterBottom style={{marginTop: 8}}>
              Maximum RAM: {ram} GB
            </Typography>
            <Slider value={ram} step={0.5} min={1} max={4} valueLabelDisplay="auto"
                    onChange={handleRamChange} marks={marks}
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={handleCloseDialog}>
              Cancel
            </Button>
            <Button onClick={handleClickOpenDialog}>
              Clear
            </Button>
            <Button type="submit" onClick={handleCreateServer} color="primary" disabled={buttonDisabled}>
              Create
            </Button>
          </DialogActions>
        </form>
      </Dialog>
    </div>
  )
}