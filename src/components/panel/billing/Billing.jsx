import {Container, Typography} from "@material-ui/core";
import React from "react";

export default function Billing() {
  return (
    <Container maxWidth="lg">
      <Typography component="h1" variant="h3" align="center" color="textPrimary" gutterBottom>
        Billing
      </Typography>
    </Container>
  )
}