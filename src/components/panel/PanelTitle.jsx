import {Typography} from "@material-ui/core";
import React from "react";

export default function PanelTitle(props) {
  return (
    <Typography component="h1" variant="h3" align="center" color="textPrimary" gutterBottom style={{marginTop: 16}}>
      {props.text}
    </Typography>
  )
}