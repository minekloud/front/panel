import React, {useEffect, useRef, useState} from "react";
import {useDispatch, useSelector} from "react-redux";

import {Box, Container, Grid, IconButton, Paper, TextField} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import PanelTitle from "../PanelTitle";
import SocketService from "../../../services/Socket.service";
import {Check, ChevronRight, Error, Send} from "@material-ui/icons";
import APIService from "../../../services/API.service";
import {ServerService} from "../../../services/Server.service";
import {clearLogAction, newLogAction, oldLogAction} from "../../../actions/logActions";

const useStyles = makeStyles((theme) => ({
  console: {
    height: "70vh",
    display: "flex",
    flexDirection: "column",
    padding: theme.spacing(2),
  },
  command: {
    marginTop: theme.spacing(4),
    padding: theme.spacing(2),
  },
  monospace: {
    fontFamily: 'monospace',
  },
  grow: {
    flexGrow: 1,
  },
  commandIcon: {
    marginTop: theme.spacing(2),
  },
  sendButton: {
    marginTop: theme.spacing(1),
  },
  commandSuccess: {
    '& label, & .MuiFormLabel-root': {
      color: 'green'
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: 'green',
    },
  },
  colorSuccess: {
    color: 'green',
  },
  colorError: {
    color: 'red',
  },
}))

export default function LogConsole() {
  const userData = useSelector((state) => state.userReducer).value
  const server = useSelector((state) => state.serverReducer).value
  const logs = useSelector((state) => state.logReducer).value

  const [socket, setSocket] = useState(null)
  const [socketConnected, setSocketConnected] = useState(false)
  const [scrollLock, setScrollLock] = useState(false)
  const [command, setCommand] = useState("")
  const [commandError, setCommandError] = useState(null)
  const [commandSuccess, setCommandSucess] = useState(false)
  const [buttonDisabled, setButtonDisabled] = useState(false)

  const consoleRef = useRef()
  const classes = useStyles()
  const dispatch = useDispatch()

  useEffect(() => {
    SocketService.connectLogSocket(userData.user.id, server.id, userData.token).then(setSocket)
    const box = consoleRef.current
    box.scrollTop = box.scrollHeight
  }, []);

  useEffect(() => {
    if (!socket) return

    socket.on("connect", () => {
      setSocketConnected(socket.connected)
      socket.emit("new-server-log")
    })
    socket.on("disconnect", () => {
      setSocketConnected(socket.connected)
      dispatch(clearLogAction())
    });
    socket.on("old-server-log", handleOldLog);
    socket.on("server-log", handleNewLog);

    socket.connect()

    return () => {
      SocketService.disconnectLogSocket()
    }
  }, [socket])

  const handleOldLog = (data) => {
    if (data.length > 0) {
      const box = consoleRef.current
      let previous = getScrollPosition(box);
      dispatch(oldLogAction(data))
      restoreScrollPosition(box, previous)
      setScrollLock(false)
    }
  }

  const handleNewLog = (data) => {
    if (data.length > 0) {
      const box = consoleRef.current
      const bottom = isScrollbarAtBottom(box)
      dispatch(newLogAction(data))
      if (bottom) {
        setScrollbarAtBottom(box)
      }
    }
  }

  const handleConsoleScroll = () => {
    const box = consoleRef.current
    if (!scrollLock && isScrollbarAtTop(box)) {
      setScrollLock(true)
      socket?.emit("old-server-log");
    }
  }

  const getScrollPosition = (box) => {
    return box.scrollHeight - box.scrollTop
  }

  const restoreScrollPosition = (box, previous) => {
    box.scrollTop = box.scrollHeight - previous;
  }

  const isScrollbarAtBottom = (box) => {
    return box.offsetHeight + box.scrollTop >= box.scrollHeight
  }

  const isScrollbarAtTop = (box) => {
    return box.scrollTop === 0
  }

  const setScrollbarAtBottom = (box) => {
    box.scrollTop = box.scrollHeight
  }

  const handleCommandChange = (event) => {
    const newCommand = event.target.value
    setCommand(newCommand)
    if (command !== newCommand && newCommand !== "") {
      setCommandError(null)
      setCommandSucess(false)
    }
  }

  const onSendCommand = (event) => {
    event.preventDefault()
    setCommandError(null)
    setCommandSucess(false)
    if (command === "") {
      setCommandError("Please enter a command.")
      return
    }
    setButtonDisabled(true)

    APIService.fetchJson(dispatch, ServerService.sendCommand(server.id, command, userData.token))
      .success(handleCommandSuccess)
      .error(handleCommandError)
      .finally(() => setButtonDisabled(false))
  }

  const handleCommandSuccess = () => {
    setCommandSucess(true)
    setCommand("")
  }

  const handleCommandError = (response, json) => {
    json().then((err) => {
      if (Array.isArray(err.message)) err.message = err.message[0]
      setCommandError(err.message);
    })
  }

  const CommandIcon = () => {
    return commandSuccess
      ? <Check className={classes.commandIcon + " " + classes.colorSuccess}/>
      : commandError !== null
        ? <Error className={classes.commandIcon + " " + classes.colorError}/>
        : <ChevronRight className={classes.commandIcon}/>
  }

  const CommandButton = () => {
    const c = [classes.sendButton]
    if (commandSuccess) {
      c.push(classes.colorSuccess)
    } else if (commandError !== null) {
      c.push(classes.colorError)
    }
    return (
      <IconButton type="submit" onClick={onSendCommand} disabled={buttonDisabled} color="primary"
                  className={c.join(" ")}>
        <Send/>
      </IconButton>
    )
  }

  return (
    <Container maxWidth="lg">
      <PanelTitle text="Console"/>
      <Paper onWheel={handleConsoleScroll} className={classes.console}>
        <Box flex={1} overflow="auto" ref={consoleRef}>
          {!socketConnected && (
            <p>Connecting to server...</p>
          )}
          {socketConnected && logs.length === 0 && (
            <p>No log found.</p>
          )}
          {logs?.map((log, index) => (
            <p key={index} className={classes.monospace}>
              {log}
            </p>
          ))}
        </Box>
      </Paper>
      <Paper className={classes.command}>
        <form onSubmit={onSendCommand}>
          <Grid container spacing={1} alignItems="flex-start">
            <Grid item>
              <CommandIcon/>
            </Grid>
            <Grid item className={classes.grow}>
              <TextField label={commandSuccess ? "Command sent! " : "Command"} size="small" fullWidth
                         className={commandSuccess ? classes.commandSuccess : ""}
                         inputProps={{className: classes.monospace}}
                         style={{fontSize: '1.4em'}} value={command} onChange={handleCommandChange}
                         error={commandError !== null} helperText={commandError}/>
            </Grid>
            <Grid item>
              <CommandButton/>
            </Grid>
          </Grid>
        </form>
      </Paper>
    </Container>
  );
};
