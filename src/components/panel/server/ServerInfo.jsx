import {Box, Button, ButtonGroup, Container, Grid, Paper, Typography} from "@material-ui/core";
import React, {useEffect, useState} from "react";
import {Delete, Description, PlayArrow, Settings, Stop} from "@material-ui/icons";
import PanelTitle from "../PanelTitle";
import DeleteServerDialog from "./status/DeleteServerDialog";
import {useDispatch, useSelector} from "react-redux";
import {makeStyles} from "@material-ui/core/styles";
import {NavLink} from "react-router-dom";
import APIService from "../../../services/API.service";
import {ServerService, ServerStatus} from "../../../services/Server.service";
import {updateServerInListAction} from "../../../actions/serverListActions";
import {updateServerAction} from "../../../actions/serverActions";
import ServerStatusIndicator from "./status/ServerStatusIndicator";
import {getStatus, MetricsService, ServerHealth} from "../../../services/Metrics.service";
import {CartesianGrid, Line, LineChart, ResponsiveContainer, Tooltip, XAxis, YAxis} from "recharts";
import moment from "moment";

const useStyles = makeStyles((theme) => ({
  paper: {
    padding: theme.spacing(2),
  },
  statusBox: {
    marginTop: theme.spacing(2),
    display: "flex",
    justifyContent: "center",
  },
  statusPaper: {
    display: "flex",
    alignItems: "center",
    padding: theme.spacing(0.5, 1.5, 0.5, 1),
    gap: theme.spacing(1),
    color: "white"
  },
  bold: {
    fontWeight: "bold",
  },
  buttonBox: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    gap: theme.spacing(2),
  },
  statusButton: {
    margin: theme.spacing(2, 0),
  },
  filterButtons: {
    marginBottom: theme.spacing(2),
  },
  bottomButtonBox: {
    marginTop: theme.spacing(2),
  },
}))

const HOURLY = 'hourly'
const DAILY = 'daily'
const WEEKLY = 'weekly'
const MONTHLY = 'monthly'
const YEARLY = 'yearly'

export default function ServerInfo() {
  const token = useSelector(state => state.userReducer).value.token
  const server = useSelector(state => state.serverReducer).value
  const health = useSelector(state => state.metricsReducer.health).find(h => h.serverId === server.id)
  const [filter, setFilter] = useState(HOURLY)
  const [rangeData, setRangeData] = useState(null)
  const [openDialog, setOpenDialog] = useState(0)
  const classes = useStyles()
  const dispatch = useDispatch()

  const serverOn = server.status === ServerStatus.ON
  const status = getStatus(server.status, health?.status)

  useEffect(() => {
    updateMetrics()
  }, [filter])

  const toggleServer = (status) => {
    APIService.fetchJson(dispatch, ServerService.update(server.id, {status: status}, token))
      .success(async (_, json) => await json().then(res => {
        dispatch(updateServerAction(res))
        dispatch(updateServerInListAction(res))
      }))
      .error(() => {
        throw new Error("A server error occured while updating the server.")
      })
  }

  const updateMetrics = () => {
    const [start, end, offset, unit, gap] = getFilter()
    MetricsService.getRange(server.id, token, start, end, offset, unit).then(res => handleMetrics(res, start, end, gap)).catch(console.error)
  }

  const handleMetrics = (data, start, end, gap) => {
    setRangeData({
      players: mapMetrics(data.players, e => ({time: e.timestamp * 1000, value: e.value}), start, end, gap),
      status: mapMetrics(data.status, e => ({
        time: e.timestamp * 1000,
        value: e.value === ServerHealth.HEALTHY ? 1 : 0
      }), start, end, gap),
      cpu: mapMetrics(data.cpu, e => ({time: e.timestamp * 1000, value: (+e.value).toFixed(2)}), start, end, gap),
      ram: mapMetrics(data.ram, e => ({
        time: e.timestamp * 1000,
        value: (e.value / 1000000000).toFixed(2)
      }), start, end, gap),
    })
  }

  const mapMetrics = (data, map, start, end, gap) => {
    const sortedData = data.map(map).sort((a, b) => a.time > b.time)
    let prevTime = start * 1000
    for (let index = 0; prevTime <= end * 1000; index++) {
      if (index >= sortedData.length) {
        sortedData.push({time: prevTime, value: 0})
      }
      const data = sortedData[index]
      if (data.time > prevTime) {
        sortedData.splice(index, 0, {time: prevTime, value: 0})
      }
      prevTime += gap * 1000
    }
    return sortedData
  }

  const getFilter = () => {
    const now = Math.floor(Date.now() / 1000)
    switch (filter) {
      case HOURLY:
        return [now - (60 * 60), now, 1, 'm', 60]
      case DAILY:
        return [now - (60 * 60 * 24), now, 20, 'm', 60 * 20]
      case WEEKLY:
        return [now - (60 * 60 * 24 * 7), now, 4, 'h', 60 * 60 * 2]
      case MONTHLY:
        return [now - (60 * 60 * 24 * 30), now, 12, 'h', 60 * 60 * 12]
      case YEARLY:
      default:
        return [now - (60 * 60 * 24 * 365), now, 5, 'd', 60 * 60 * 24 * 5]
    }
  }

  const getFormatter = (time) => {
    switch (filter) {
      case HOURLY:
      case DAILY:
        return moment(time).format('HH:mm')
      case WEEKLY:
      case MONTHLY:
      case YEARLY:
      default:
        return moment(time).format('DD/MM')
    }
  }

  const getFullFormatter = (time) => {
    return moment(time).format('DD/MM/yyyy HH:mm:ss')
  }

  const buttonVariant = (_filter) => {
    return _filter === filter ? "contained" : null
  }

  const StatusButton = (props) => serverOn
    ? <Button variant="contained" size="large" color="secondary" className={classes.statusButton}
              startIcon={<Stop/>} onClick={() => toggleServer(ServerStatus.OFF)} {...props}>Stop Server</Button>
    : <Button variant="contained" size="large" className={classes.statusButton}
              style={{color: "white", backgroundColor: "green"}}
              startIcon={<PlayArrow/>} onClick={() => toggleServer(ServerStatus.ON)} {...props}>Start Server</Button>

  const Graph = (props) => {
    return (
      <Grid item xs={12} md={6}>
        <Paper className={classes.paper}>
          <Typography component="h2" variant="h6" color="primary" gutterBottom>
            {props.name}: {props.data && props.data.length > 0 ? props.data[props.data.length - 1].value : ''}
          </Typography>
          <ResponsiveContainer width="100%" height={250}>
            <LineChart data={props.data}>
              <XAxis dataKey="time" tickFormatter={getFormatter}/>
              <YAxis dataKey="value" domain={props.domain}/>
              <CartesianGrid stroke="#eee" strokeDasharray="5 5"/>
              <Tooltip labelFormatter={getFullFormatter}/>
              <Line type="monotone" dataKey="value" stroke={props.color} isAnimationActive={false}/>
            </LineChart>
          </ResponsiveContainer>
        </Paper>
      </Grid>
    )
  }

  return (
    <Container maxWidth="lg">
      <PanelTitle text="Status"/>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Paper className={classes.paper}>
            <Typography variant="h5" align="center" className={classes.bold}>
              {server.hostname}.play.minekloud.com
            </Typography>
            <Box className={classes.statusBox}>
              <ServerStatusIndicator status={status}/>
            </Box>
          </Paper>
        </Grid>
        <Grid item xs={12}>
          <Box className={classes.buttonBox}>
            <StatusButton/>
          </Box>
        </Grid>
        <Graph name="Players" data={rangeData?.players} domain={[0, 'auto']} color="purple"/>
        <Graph name="Status (Up/Down)" data={rangeData?.status} domain={[0, 1]} color="green"/>
        <Graph name="CPU Usage (%)" data={rangeData?.cpu} domain={[0, 100]} color="red"/>
        <Graph name="RAM Usage (GB)" data={rangeData?.ram} domain={[0, +server.maxAvailableRAM]} color="blue"/>
        <Grid item xs={12}>
          <Box className={[classes.buttonBox, classes.filterButtons].join(' ')}>
            <ButtonGroup color="primary" aria-label="outlined primary button group">
              <Button variant={buttonVariant(HOURLY)} onClick={() => setFilter(HOURLY)}>Hourly</Button>
              <Button variant={buttonVariant(DAILY)} onClick={() => setFilter(DAILY)}>Daily</Button>
              <Button variant={buttonVariant(WEEKLY)} onClick={() => setFilter(WEEKLY)}>Weekly</Button>
              <Button variant={buttonVariant(MONTHLY)} onClick={() => setFilter(MONTHLY)}>Monthly</Button>
              <Button variant={buttonVariant(YEARLY)} onClick={() => setFilter(YEARLY)}>Yearly</Button>
            </ButtonGroup>
          </Box>
        </Grid>
        <Grid item xs={12}>
          <Paper className={classes.paper}>
            <Typography variant="h5" align="center" className={classes.bold}>
              Type : Vanilla
            </Typography>
            <Typography variant="h5" align="center" className={classes.bold}>
              Version : 1.16.4
            </Typography>
            <Box className={[classes.buttonBox, classes.bottomButtonBox].join(' ')}>
              <NavLink to="/panel/server/configuration">
                <Button variant="contained" color="default" startIcon={<Settings/>}>
                  Configure
                </Button>
              </NavLink>
              <NavLink to="/panel/server/console">
                <Button variant="contained" color="default" startIcon={<Description/>}>
                  Console
                </Button>
              </NavLink>
              <Button variant="contained" color="secondary" startIcon={<Delete/>}
                      onClick={() => setOpenDialog(prev => prev + 1)}>
                Delete
              </Button>
            </Box>
          </Paper>
        </Grid>
      </Grid>
      <DeleteServerDialog openDialog={openDialog}/>
    </Container>
  )
}