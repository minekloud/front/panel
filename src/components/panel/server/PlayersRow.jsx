import {Button, Grid, makeStyles, TableCell, TableRow} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import RemoveCircleOutlineIcon from '@material-ui/icons/RemoveCircleOutline';
import FaceIcon from '@material-ui/icons/Face';
import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {useHistory} from "react-router-dom";
import APIService from "../../../services/API.service";
import {ServerService} from "../../../services/Server.service";


const useStyle = makeStyles(() => ({
  serverRow: {
    cursor: "pointer"
  },
}));

const defaultTooltipText = "Copy address to clipboard"



export  function PlayersRow(props) {


  const dispatch = useDispatch()
  const history = useHistory()
  const classes = useStyle()
  const loggedUser = useSelector((state) => state.userReducer).value
  const [avatarURL, setAvatarURL] = useState("") 
  const server = useSelector((state) => state.serverReducer).value
  const urlAvatar = `https://minotar.net/avatar/${props.user.username}`


  


  // useEffect(() => {


  //   APIService.fetchJson(dispatch, ServerService.getPlayerAvatar(props.user.username))
  //         .success(async (_,json) => await json().then(res => {
  //           setAvatarURL("https://minotar.net/avatar/" + res.id)
  //           console.log(res.id)
  //         }))
    
  // }, [loggedUser]);


  const handleCommand = (e) => {
    let command = e.currentTarget.value + ` ${props.user.username}`
    APIService.fetchJson(dispatch, ServerService.sendCommand(server.id, command, loggedUser.token))
      .success(res => console.log(res))
      .error(err => {
        throw new Error(`A server error occured while performing the command : ${command}.`)
      })
    
  }

  


  return (
    <TableRow hover={true}  className={classes.serverRow}>
      <TableCell><img src={urlAvatar} width="32" height="32" alt={avatarURL}/></TableCell>
      <TableCell>{props.user.username}</TableCell>
      <TableCell>
        <Grid container spacing={2}>
          <Grid item>
            <Button variant="contained" color="primary" value="/kick" startIcon={<RemoveCircleOutlineIcon/>} onClick={handleCommand}>
              Kick
            </Button>
            </Grid>
            <Grid item>
            <Button variant="contained" color="secondary" startIcon={<DeleteIcon/>} value="/ban" onClick={handleCommand}>
              Ban
            </Button>
            </Grid>
            <Grid item>
            <Button variant="outlined" color="default" startIcon={<FaceIcon/>} value="/op" onClick={handleCommand}>
              OP
            </Button>
          </Grid>
        </Grid>
      </TableCell>
    </TableRow>


  );
}

