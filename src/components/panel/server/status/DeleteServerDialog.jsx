import {Button, Dialog, DialogActions, DialogContent} from "@material-ui/core";
import DialogContentText from "@material-ui/core/DialogContentText";
import React, {useEffect, useState} from "react";
import {ServerService} from "../../../../services/Server.service";
import {showInfoAlertAction} from "../../../../actions/alertActions";
import {fetchServerListAction} from "../../../../actions/serverListActions";
import {useDispatch, useSelector} from "react-redux";
import {useHistory} from "react-router-dom";
import {CustomDialogTitle} from "../../../CustomDialogTitle";
import APIService from "../../../../services/API.service";

export default function DeleteServerDialog(props) {
  const server = useSelector((state) => state.serverReducer).value
  const [openDialog, setOpenDialog] = useState(false)
  const [buttonDisabled, setButtonDisabled] = useState(false)

  const dispatch = useDispatch()
  const history = useHistory()

  useEffect(() => {
    if (props.openDialog > 0) {
      setOpenDialog(true)
    }
  }, [props.openDialog])

  const handleServerDelete = () => {
    setButtonDisabled(true)
    APIService.fetchJson(dispatch, ServerService.delete(server.id))
      .success(onServerDeleteSuccess)
      .error(() => {
        throw new Error("A server error occured while deleting the server.")
      })
      .finally(handleDialogClose)
  }

  const onServerDeleteSuccess = () => {
    dispatch(fetchServerListAction())
    dispatch(showInfoAlertAction("Your server has been deleted.", 4000))
    history.push("/panel/list")
  }

  const handleDialogClose = () => {
    setButtonDisabled(false)
    setOpenDialog(false)
  };

  return (
    <Dialog open={openDialog} onClose={handleDialogClose}>
      <CustomDialogTitle onClose={handleDialogClose}>
        Delete server
      </CustomDialogTitle>
      <DialogContent>
        <DialogContentText>
          Are you sure you want to delete this server? This action can't be undone.
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleDialogClose}>
          Cancel
        </Button>
        <Button disabled={buttonDisabled} onClick={handleServerDelete} color="secondary" autoFocus>
          Delete
        </Button>
      </DialogActions>
    </Dialog>
  )
}