import {CircularProgress, Paper, Tooltip, Typography} from "@material-ui/core";
import {PlayArrow, Replay, Stop} from "@material-ui/icons";
import React from "react";
import {Status} from "../../../../services/Metrics.service";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  statusIndicatorPaper: {
    display: "flex",
    color: "white",
    alignItems: "center",
  },
  statusIndicatorPaperNormal: {
    padding: theme.spacing(0.5, 1.5),
    gap: theme.spacing(1),
  },
  statusIndicatorPaperSmall: {
    padding: theme.spacing(0.5),
  },
  statusIndicatorText: {
    fontWeight: "bold",
  },
  bgGreen: {
    backgroundColor: "green",
  },
  bgRed: {
    backgroundColor: "red",
  },
  bgOrange: {
    backgroundColor: "orange",
  },
  bgGrey: {
    backgroundColor: "lightgray",
    color: "black"
  }
}))

export default function ServerStatusIndicator(props) {
  const status = props.status
  const size = props.size || 'normal'
  const classes = useStyles()

  const StatusIcon = () => {
    switch (status) {
      case Status.ON:
        return <PlayArrow/>
      case Status.OFF:
        return <Stop/>
      case Status.UNKNOWN:
        return <CircularProgress size={30}/>
      default:
        return <Replay/>
    }
  }

  const getBackgroundClass = () => {
    switch (status) {
      case Status.ON:
        return classes.bgGreen
      case Status.OFF:
      case Status.STOPPING:
        return classes.bgRed
      case Status.STARTING:
        return classes.bgOrange
      case Status.UNKNOWN:
      default:
        return classes.bgGrey
    }
  }

  const getText = () => {
    switch (status) {
      case Status.STARTING:
        return 'Starting'
      case Status.STOPPING:
        return 'Stopping'
      case Status.UNKNOWN:
        return 'Unknown'
      default:
        return status
    }
  }

  if (size === 'small') {
    return (
      <Tooltip title={getText()} placement="top" arrow>
        <Paper
          className={[classes.statusIndicatorPaper, classes.statusIndicatorPaperSmall, getBackgroundClass()].join(' ')}>
          <StatusIcon/>
        </Paper>
      </Tooltip>
    )
  }
  return (
    <Paper
      className={[classes.statusIndicatorPaper, classes.statusIndicatorPaperNormal, getBackgroundClass()].join(' ')}>
      <StatusIcon/>
      {status !== Status.UNKNOWN && (
        <Typography variant="h6" className={classes.statusIndicatorText}>{getText()}</Typography>)}
    </Paper>
  )
}

