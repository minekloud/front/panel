import {Box, Button, CircularProgress, Container, Grid, Paper, Typography} from "@material-ui/core";
import PanelTitle from "../PanelTitle";
import React, {useEffect, useState} from "react";
import {makeStyles} from "@material-ui/core/styles";
import {CartesianGrid, Line, LineChart, ResponsiveContainer, Tooltip, XAxis, YAxis} from "recharts";
import {useDispatch, useSelector} from "react-redux";
import APIService from "../../../services/API.service";
import {PredictionService} from "../../../services/Prediction.service";
import moment from "moment";
import {Refresh} from "@material-ui/icons";

const useStyles = makeStyles((theme) => ({
  paper: {
    padding: theme.spacing(2),
  },
  buttonBox: {
    display: "flex",
    justifyContent: "center",
  },
}))

export default function Projections() {
  const token = useSelector(state => state.userReducer).value.token
  const server = useSelector(state => state.serverReducer).value
  const [loading, setLoading] = useState(true)
  const [data, setData] = useState(null)
  const dispatch = useDispatch()
  const classes = useStyles()

  useEffect(() => {
    if (loading === true) {
      APIService.fetchJson(dispatch, PredictionService.get(server.id, token))
        .success((_, json) => json().then(setData))
        .finally(() => setLoading(false))
    }
  }, [loading])

  const handleReload = () => {
    setLoading(true)
    setData(null)
  }

  return (
    <Container maxWidth="lg">
      <PanelTitle text="Projections"/>
      <Grid container spacing={3} alignItems="center">
        <Grid item xs={12}>
          <Paper className={classes.paper}>
            <Typography component="h2" variant="h6" color="primary" gutterBottom>
              Players
            </Typography>
            {data?.length > 0 && (
              <ResponsiveContainer width="100%" height={600}>
                <LineChart data={data}>
                  <XAxis dataKey="timestamp" tickFormatter={time => moment(time).format('DD/MM')}/>
                  <YAxis dataKey="nbPlayer"/>
                  <CartesianGrid stroke="#eee" strokeDasharray="5 5"/>
                  <Tooltip labelFormatter={time => moment(time).format('DD/MM/yyyy HH:mm:ss')}/>
                  <Line type="monotone" dataKey="nbPlayer" isAnimationActive={false}/>
                </LineChart>
              </ResponsiveContainer>
            )}
            {data?.length === 0 && (
              <Typography gutterBottom>
                Not enough data.
              </Typography>
            )}
            {loading && (
              <div>
                <Typography gutterBottom>
                  Loading projection... (can take a few seconds)
                </Typography>
                <CircularProgress style={{marginTop: 20}}/>
              </div>
            )}
          </Paper>
        </Grid>
        <Grid item xs={12}>
          <Box className={classes.buttonBox}>
            <Button variant="contained" size="large" color="primary"
                    onClick={handleReload} startIcon={<Refresh/>} disabled={loading}>
              Reload
            </Button>
          </Box>
        </Grid>
      </Grid>
    </Container>
  )
}
