import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {
  CircularProgress,
  Container,
  makeStyles,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography
} from '@material-ui/core';
import {PlayersRow} from './PlayersRow'
import PanelTitle from "../PanelTitle";
import APIService from "../../../services/API.service";
import {ServerService} from "../../../services/Server.service";

const useStyle = makeStyles(() => ({
  table: {
    width: "100%",
  },
}));


export default function Players() {
  const server = useSelector((state) => state.serverReducer).value
  const loggedUser = useSelector((state) => state.userReducer).value
  const [updatingList, setUpdatingList] = useState(true)
  const [listeJoueurs, setListeJoueurs] = useState(null)
  const classes = useStyle();
  const dispatch = useDispatch()

  useEffect(() => {
    const interval = setInterval(() => {
      APIService.fetchJson(dispatch, ServerService.getListUsers(server.id, loggedUser.token))
        .success(async (_, json) => await json().then(res => {
          setListeJoueurs([...res])
        }))
        .finally(() => setUpdatingList(false))
    }, 1000);
    return () => clearInterval(interval);
  })

  return (
    <Container maxWidth="lg">
      <PanelTitle text="Players"/>
      <TableContainer component={Paper}>
        <Table className={classes.table}>
          <TableHead>
            <TableRow>
              <TableCell>Skin</TableCell>
              <TableCell>Username</TableCell>
              <TableCell>Action</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {listeJoueurs?.map((user) => (
              <PlayersRow key={listeJoueurs.indexOf(user)} user={user}/>
            ))}
            {updatingList && (
              <TableRow>
                <TableCell colSpan={3} align="center">
                  <CircularProgress/>
                </TableCell>
              </TableRow>
            )}
            {!updatingList && listeJoueurs?.length === 0 && (
              <TableRow>
                <TableCell colSpan={6} align="center">
                  <Typography>
                    No player connected.
                  </Typography>
                </TableCell>
              </TableRow>
            )}
          </TableBody>
        </Table>
      </TableContainer>
    </Container>
  )
}
