import React, {useState} from "react";
import PanelTitle from "../PanelTitle";
import {
  Button,
  Container,
  FormControl,
  FormControlLabel,
  FormGroup,
  Grid,
  InputLabel,
  MenuItem,
  Paper,
  Select,
  Slider,
  Switch,
  Tooltip,
  Typography
} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {makeStyles} from "@material-ui/core/styles";
import APIService from "../../../services/API.service";
import {ServerService} from "../../../services/Server.service";
import {updateServerAction} from "../../../actions/serverActions";
import {updateServerInListAction} from "../../../actions/serverListActions";
import {Save} from "@material-ui/icons";
import ServerRebootDialog from "./configuration/ServerRebootDialog";
import {showSuccessAlertAction} from "../../../actions/alertActions";

const useStyles = makeStyles((theme) => ({
  paper: {
    padding: theme.spacing(2),
  },
  ramLabel: {
    marginTop: theme.spacing(1),
  }
}))

export default function Configuration() {
  const server = useSelector(state => state.serverReducer).value
  const token = useSelector(state => state.userReducer).value.token
  const [initialRam, setInitialRam] = useState(server.maxAvailableRAM)
  const [ram, setRam] = useState(server.maxAvailableRAM)
  const [autoStop, setAutoStop] = useState(server.autoStop)
  const [autoStart, setAutoStart] = useState(server.autoStart)
  const [openDialog, setOpenDialog] = useState(0)
  const [dialogData, setDialogData] = useState({})
  const classes = useStyles()
  const dispatch = useDispatch()

  const marks = [
    {
      value: 1,
      label: '1 GB',
    },
    {
      value: 2,
      label: '2 GB',
    },
    {
      value: 3,
      label: '3 GB',
    },
    {
      value: 4,
      label: '4 GB',
    },
  ]

  const handleRamChange = (_, value) => {
    setRam(value)
  }

  const handleServerChange = (e) => {
    e.preventDefault()
    if (server.status === 'on') {
      setDialogData({maxAvailableRAM: ram})
      setOpenDialog(prev => prev + 1)
      return
    }
    updateServer({maxAvailableRAM: ram})
      .success(() => {
        setInitialRam(ram)
        dispatch(showSuccessAlertAction("Server updated!", 4000))
      })
  }

  const dialogRebootSuccess = () => {
    setInitialRam(ram)
  }

  const toggleAutoStop = (event) => {
    updateServer({autoStop: event.target.checked})
  }

  const toggleAutoStart = (event) => {
    updateServer({autoStart: event.target.checked})
  }

  const updateServer = (data) => {
    return APIService.fetchJson(dispatch, ServerService.update(server.id, data, token))
      .success(async (_, json) => await json().then(res => {
        setAutoStop(res.autoStop)
        setAutoStart(res.autoStart)
        dispatch(updateServerAction(res))
        dispatch(updateServerInListAction(res))
      }))
      .error(() => {
        throw new Error("A server error occured while updating the server.")
      })
  }
  return (
    <Container maxWidth="lg">
      <PanelTitle text="Configuration"/>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Paper className={classes.paper}>
            <Typography component="h2" variant="h6" color="primary" gutterBottom>
              Minecraft
            </Typography>
            <form>
              <FormControl variant="outlined" margin="normal" fullWidth>
                <InputLabel>Type</InputLabel>
                <Select value="Vanilla" label="Type">
                  <MenuItem value="Vanilla">Vanilla</MenuItem>
                  <MenuItem disabled>Spigot</MenuItem>
                  <MenuItem disabled>Paper</MenuItem>
                </Select>
              </FormControl>
              <FormControl variant="outlined" margin="normal" fullWidth>
                <InputLabel>Version</InputLabel>
                <Select value="1.16.4" label="Version">
                  <MenuItem value="1.16.4">1.16.4</MenuItem>
                  <MenuItem disabled>1.16.3</MenuItem>
                  <MenuItem disabled>1.16.2</MenuItem>
                  <MenuItem disabled>1.16.1</MenuItem>
                  <MenuItem disabled>1.16</MenuItem>
                </Select>
              </FormControl>
              <Typography gutterBottom className={classes.ramLabel}>
                Maximum RAM: {ram} GB
              </Typography>
              <Slider value={ram} step={0.5} min={1} max={4} valueLabelDisplay="auto"
                      onChange={handleRamChange} marks={marks}
              />
            </form>
            <Grid container spacing={2} justify="center" style={{marginTop: 20}}>
              <Grid item>
                <Button type="submit" margin="normal" fullWidth variant="contained" color="primary" size="large"
                        startIcon={<Save/>} disabled={ram === initialRam} onClick={handleServerChange}>
                  Save
                </Button>
              </Grid>
            </Grid>
          </Paper>
        </Grid>
        <Grid item xs={12}>
          <Paper className={classes.paper}>
            <Typography component="h2" variant="h6" color="primary" gutterBottom>
              Administration
            </Typography>
            <FormGroup row>
              <Tooltip title="The server automatically stops after 5 minutes if no player is connected."
                       placement="right" arrow>
                <FormControlLabel
                  control={
                    <Switch
                      checked={autoStop}
                      onChange={toggleAutoStop}
                      color="primary"
                    />
                  }
                  label="Auto Stop"
                />
              </Tooltip>
            </FormGroup>
            <FormGroup row>
              <Tooltip title="The server automatically starts if someone attempts to connect to the server."
                       placement="right" arrow>
                <FormControlLabel
                  control={
                    <Switch
                      checked={autoStart}
                      onChange={toggleAutoStart}
                      color="primary"
                    />
                  }
                  label="Auto Start"
                />
              </Tooltip>
            </FormGroup>
          </Paper>
        </Grid>
      </Grid>
      <ServerRebootDialog openDialog={openDialog} data={dialogData} onSuccess={dialogRebootSuccess}/>
    </Container>
  )
}