import {Button, Dialog, DialogActions, DialogContent} from "@material-ui/core";
import DialogContentText from "@material-ui/core/DialogContentText";
import React, {useEffect, useState} from "react";
import {ServerService} from "../../../../services/Server.service";
import {useDispatch, useSelector} from "react-redux";
import {CustomDialogTitle} from "../../../CustomDialogTitle";
import APIService from "../../../../services/API.service";
import {showSuccessAlertAction} from "../../../../actions/alertActions";
import {updateServerAction} from "../../../../actions/serverActions";
import {updateServerInListAction} from "../../../../actions/serverListActions";

export default function ServerRebootDialog(props) {
  const server = useSelector((state) => state.serverReducer).value
  const token = useSelector(state => state.userReducer).value.token
  const [openDialog, setOpenDialog] = useState(false)
  const [buttonDisabled, setButtonDisabled] = useState(false)

  const dispatch = useDispatch()

  useEffect(() => {
    if (props.openDialog > 0) {
      setOpenDialog(true)
    }
  }, [props.openDialog])

  const handleServerRestart = () => {
    setButtonDisabled(true)
    APIService.fetchJson(dispatch, ServerService.update(server.id, props.data, token))
      .success(async (_, json) => await json().then(onServerDeleteSuccess))
      .error(() => {
        throw new Error("A server error occured while updating the server.")
      })
      .finally(handleDialogClose)
  }

  const onServerDeleteSuccess = (res) => {
    dispatch(updateServerAction(res))
    dispatch(updateServerInListAction(res))
    dispatch(showSuccessAlertAction("Your server has been updated and will restart.", 4000))
    props.onSuccess()
  }

  const handleDialogClose = () => {
    setButtonDisabled(false)
    setOpenDialog(false)
  };

  return (
    <Dialog open={openDialog} onClose={handleDialogClose}>
      <CustomDialogTitle onClose={handleDialogClose}>
        Restart server
      </CustomDialogTitle>
      <DialogContent>
        <DialogContentText>
          Your server must be restarted to apply your changes.
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleDialogClose}>
          Cancel
        </Button>
        <Button disabled={buttonDisabled} onClick={handleServerRestart} color="primary" autoFocus>
          Restart
        </Button>
      </DialogActions>
    </Dialog>
  )
}