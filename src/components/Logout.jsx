import {useDispatch} from "react-redux";
import {logoutAction} from "../actions/userActions";
import {unselectServerAction} from "../actions/serverActions";
import {Redirect} from "react-router-dom";
import {showInfoAlertAction} from "../actions/alertActions";
import {clearServerListAction} from "../actions/serverListActions";
import {useEffect} from "react";
import {disconnectMetrics} from "../actions/metricsActions";

export default function Logout() {
  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(logoutAction())
    dispatch(unselectServerAction())
    dispatch(clearServerListAction())
    dispatch(disconnectMetrics())
    dispatch(showInfoAlertAction("User successfully logged out.", 4000))
  })
  return <Redirect to="/login"/>
}