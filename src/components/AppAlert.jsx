import {Alert} from "@material-ui/lab";
import {Snackbar} from "@material-ui/core";
import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {hideAlertAction} from "../actions/alertActions";

export default function AppAlert() {
  const alert = useSelector(state => state.alertReducer).value
  const dispatch = useDispatch()

  const handleCloseAlert = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    dispatch(hideAlertAction())
  };

  if (alert === null) return null
  return (
    <Snackbar open="true" autoHideDuration={alert.duration} onClose={handleCloseAlert}>
      <Alert elevation={6} variant="filled" onClose={handleCloseAlert} severity={alert.type}>
        {alert.message}
      </Alert>
    </Snackbar>
  )
}