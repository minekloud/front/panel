import React, {useState} from "react";
import {Avatar, Box, Button, Container, CssBaseline, Grid, makeStyles, TextField, Typography} from '@material-ui/core';
import {NavLink, Redirect, useHistory} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import Copyright from "./Copyright";
import UserService from "../services/User.service";
import {PersonAdd} from "@material-ui/icons";
import {showSuccessAlertAction} from "../actions/alertActions";
import APIService from "../services/API.service";

const useStyle = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'

  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.primary.main
  },
  form: {
    widht: "100%",
    marginTop: theme.spacing(3),
  },
}));


export default function Register() {
  const [username, setUsername] = useState("")
  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")
  const [repassword, setRepassword] = useState("")
  const [usernameError, setUsernameError] = useState(null)
  const [emailError, setEmailError] = useState(null)
  const [passwordError, setPasswordError] = useState(null)
  const [repasswordError, setRepasswordError] = useState(null)
  const [buttonDisabled, setButtonDisabled] = useState(false)
  const loggedUser = useSelector((state) => state.userReducer).value

  const classes = useStyle()
  const history = useHistory()
  const dispatch = useDispatch()

  const handleUsernameChange = (event) => {
    let newUsername = event.target.value
    setUsername(newUsername)
    if (usernameError !== null && username !== newUsername && newUsername !== "") {
      setUsernameError(null)
    }
  }

  const handleEmailChange = (event) => {
    let newEmail = event.target.value
    setEmail(newEmail)
    if (emailError !== null && email !== newEmail && newEmail !== "") {
      setEmailError(null)
    }
  }

  const handlePasswordChange = (event) => {
    let newPassword = event.target.value
    setPassword(newPassword)
    if (passwordError !== null && password !== newPassword && newPassword !== "") {
      setPasswordError(null)
    }
  }

  const handleRepasswordChange = (event) => {
    let newRepassword = event.target.value
    setRepassword(newRepassword)
    if (repasswordError !== null && repassword !== newRepassword && newRepassword !== "") {
      setRepasswordError(null)
    }
  }

  const validateUsername = () => {
    if (username === "") {
      setUsernameError("Please enter a username.")
      return false
    }
    if (username.length < 3 || username.length > 16) {
      setUsernameError("Your username must contain between 3 and 16 characters.")
      return false
    }
    return true
  }

  const validateEmail = () => {
    if (email === "") {
      setEmailError("Please enter your email address.")
      return false
    }
    const regexEmail = /\S+@\S+\.\S+/
    if (!regexEmail.test(email)) {
      setEmailError("Please enter a valid email address.")
      return false
    }
    return true
  }

  const validatePassword = () => {
    if (password === "") {
      setPasswordError("Please enter a password.")
      return false
    }
    const regexPwd = /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(\S){8,}$/
    if (!regexPwd.test(password)) {
      setPasswordError("Your password must contain at least 1 lowercase character," +
        " 1 uppercase character, 1 digit, and must be at least 8 characters long.")
      return false
    }
    return true
  }

  const validateRepassword = () => {
    if (repassword === "") {
      setRepasswordError("Please enter your password again.")
      return false
    }
    if (password !== repassword) {
      setRepasswordError("Both passwords must be equal.")
      return false
    }
    return true
  }

  const validateForm = () => [validateUsername(), validateEmail(), validatePassword(), validateRepassword()].every(Boolean)

  const onRegisterClick = async (e) => {
    e.preventDefault()
    clearErrors()
    if (!validateForm()) return
    setButtonDisabled(true)

    APIService.fetchJson(dispatch, UserService.register(username, password, email))
      .success(onRegisterSuccess)
      .error(onRegisterError)
      .finally(() => setButtonDisabled(false))
  }

  const onRegisterSuccess = () => {
    dispatch(showSuccessAlertAction("Account successfully registered! You can now login to your account.", 6000))
    history.push("/login")
  }

  const onRegisterError = async (response, getError) => {
    if (response.status === 403) {
      throw new Error("Registering has been disabled by Minekloud administrators.")
    }
    await getError().then(err => {
      if (!err.message) return
      if (Object.prototype.toString.call(err.message) === "[object String]") throw new Error(err.message)
      err.message.forEach(msg => {
        switch (msg.field) {
          case 'username':
            setUsernameError(msg.message)
            break
          case 'email':
            setEmailError(msg.message)
            break;
          default:
            throw new Error(msg)
        }
      })
    })
  }

  const onCancelClick = () => {
    setUsername("");
    setEmail("");
    setPassword("");
    setRepassword("");
    clearErrors()
  }

  const clearErrors = () => {
    setUsernameError(null)
    setEmailError(null)
    setPasswordError(null)
    setRepasswordError(null)
  }

  if (loggedUser != null) {
    return <Redirect to="/panel"/>
  }
  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline/>
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <PersonAdd/>
        </Avatar>
        <Typography component="h1" variant="h5">
          Register
        </Typography>
        <form className={classes.root} onSubmit={onRegisterClick}>
          <TextField margin="normal" fullWidth required variant="outlined" id="standard-username" label="Username"
                     value={username} onChange={handleUsernameChange}
                     error={usernameError !== null} helperText={usernameError}/>
          <TextField margin="normal" fullWidth required variant="outlined" type="email" id="standard-email"
                     label="Email" value={email} onChange={handleEmailChange}
                     error={emailError !== null} helperText={emailError}/>
          <TextField margin="normal" fullWidth required variant="outlined" type="password" id="standard-password"
                     value={password} label="Password" onChange={handlePasswordChange}
                     error={passwordError !== null} helperText={passwordError}/>
          <TextField margin="normal" fullWidth required variant="outlined" type="password" id="standard-re-password"
                     value={repassword} label="Re-Password" onChange={handleRepasswordChange}
                     error={repasswordError !== null} helperText={repasswordError}/>
          {/* <FormControlLabel control={<Checkbox value="CGU" color="primary" />} label="Agree to CGU"/> */}
          <Grid container spacing={3}>
            {/* <Grid item xs>
                <Link href="#" variant="body2">
                  Forgot password?
                </Link>
              </Grid> */}
            <Grid item>
              <NavLink to="/login">
                <Typography variant="body2" color="primary" className="MuiLink-underlineHover">
                  Already have an account? Sign in
                </Typography>
              </NavLink>
            </Grid>
          </Grid>
          <div>
            <Grid container spacing={2} justify="center" style={{marginTop: 20}}>
              <Grid item>
                <Button type="submit" margin="normal" fullWidth variant="contained" color="primary" size="large"
                        startIcon={<PersonAdd/>} onClick={onRegisterClick} disabled={buttonDisabled}>
                  Register
                </Button>
              </Grid>
              <Grid item>
                <Button margin="normal" fullWidth variant="outlined" size="large" onClick={onCancelClick}>
                  Clear
                </Button>
              </Grid>
            </Grid>
          </div>
        </form>
      </div>

      <Box mt={5}>
        <Copyright/>
      </Box>
    </Container>
  )

}
