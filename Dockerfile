FROM nginx:1.19.6-alpine

COPY entrypoint.sh /entrypoint.sh
RUN chmod +x entrypoint.sh

COPY nginx.conf /etc/nginx/conf.d/default.conf

WORKDIR /usr/share/nginx/html

COPY build .

EXPOSE 80

ENTRYPOINT [ "/entrypoint.sh" ]